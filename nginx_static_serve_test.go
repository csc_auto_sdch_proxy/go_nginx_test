package go_nginx_test

import (
	"flag"
	"log"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"net"
	"net/url"
	"net/http"
	"net/http/httptest"
	"time"
	"bytes"
	"strings"
	"encoding/json"
	"strconv"
	"testing"
	"github.com/stretchr/testify/suite"
	"bitbucket.com/go_nginx_test/static_server"
)


var nginx_exe = flag.String("nginx_exe", "/usr/local/nginx/sbin/nginx", "path nginx executable")
var nginx_conf = flag.String("nginx_conf", "/home/vladimirskipor/Development/Go/src/bitbucket.com/go_nginx_test/config/nginx_proxy.conf", "path to nginx conf")
var config_file = flag.String("config_file", "./config/static_serve_test.json", "path to JSON config file")

type Config struct {
	ProxyPort int
	ResponseForwardPort int
	PageRootdir string
}
func LoadConfig(configFileName string) (*Config, error) {
	file, err := os.Open(configFileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	dec := json.NewDecoder(file)
	config := Config{}
	if err = dec.Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil
}
var testPagesNames []string
var testPagesDir string
var proxyUrl *url.URL
var proxyPort, responseForwardPort string

func NewListnerOnPort(port string) net.Listener {
	l, err := net.Listen("tcp", "127.0.0.1:"+port)
	if err != nil {
		log.Fatalf("httptest: failed to listen on %v: %v", port, err)
	}
	return l
}

func NewTestServerOnPort(handler http.Handler, port string) *httptest.Server {
	server := &httptest.Server{
		Listener: NewListnerOnPort(port),
		Config:   &http.Server{Handler: handler},
	}
	server.Start()
	return server
}

type ServerTestSuite struct {
	suite.Suite
	handler http.Handler
	server  *httptest.Server
	client *http.Client
}

type ChunkedTestSuite ServerTestSuite
type StaticTestSuite ServerTestSuite


func (suite *ChunkedTestSuite) SetupSuite() {
	suite.handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(http.CanonicalHeaderKey("Content-Type"), "text/html")
		static_server.ChunkedHandler(w, r)
	})
	suite.server = httptest.NewServer(suite.handler)
	suite.client =  &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}
}
func (suite *ChunkedTestSuite) TearDownSuite() {
	suite.server.Close()
}


func (suite *StaticTestSuite) SetupSuite() {
	suite.handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(http.CanonicalHeaderKey("Content-Type"), "text/html")
		static_server.StaticHandler(w, r)
	})
	suite.server = httptest.NewServer(suite.handler)
	suite.client = &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}
}
func (suite *StaticTestSuite) TearDownSuite() {
	suite.server.Close()
}


func (s * ServerTestSuite) CheckAllFiles() {

	filenamesChan := make(chan string, 1)
	fileDoneChan := make(chan string, len(testPagesNames))

	//handler to emulate drop_server for response_forward
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("Response forward mock")
		s.Equal(r.Method, "POST")
		s.Equal(r.Header.Get("X-Sdch-Host"), s.server.URL[len("http://"):])
		body, err := ioutil.ReadAll(r.Body)
		s.NoError(err, "Body read error")
		filename := <-filenamesChan
		file, err := ioutil.ReadFile(filename)
		s.NoError(err, "File read error")
		s.True(bytes.Equal(body, file), "File and response are different!!")
		fileDoneChan <- filename
	})

	responseForwardTestServer := NewTestServerOnPort(h, responseForwardPort)
	defer responseForwardTestServer.Close()

	log.Println("Response forward mock on: ", responseForwardTestServer.URL)
	log.Println("Checking files...")
	log.Println("Serving server: " + s.server.URL)
	log.Println("testPagesDir: ", testPagesDir)


	for _, filename := range testPagesNames {
		log.Println("Checking ", filename)
		filenamesChan <- filename // notify, with file will be posted by response_forward
		resp, err := s.client.Get(s.server.URL + "/" + filename)
		defer resp.Body.Close()
		s.NoError(err, "Get error")
		body, err := ioutil.ReadAll(resp.Body)
		s.NoError(err, "Body read error")
		file, err := ioutil.ReadFile(filename)
		s.NoError(err, "File read error")
		s.True(bytes.Equal(body, file), "File and response are different!!")
	}

	for i := 0; i < len(testPagesNames); i++ {
		select {
		case <-fileDoneChan:
			break;
		case <-time.After(500 * time.Millisecond):
			log.Fatal("Not all files sended to response forward mock")
		}
	}

}

func (s * StaticTestSuite) TestAllFiles() {
	(*ServerTestSuite)(s).CheckAllFiles()
}

func (s * ChunkedTestSuite) TestAllFiles() {
	(*ServerTestSuite)(s).CheckAllFiles()
}


func (s *StaticTestSuite) TestHeaders() {
	req, err := http.NewRequest("GET", "/" + testPagesNames[0], nil)
	if err != nil {
		log.Fatal(err)
	}
	w := httptest.NewRecorder()
	s.handler.ServeHTTP(w, req)
	log.Println(w.Header())
	s.Equal(w.Header().Get("Content-Type"), "text/html")
	s.NotEmpty(w.Header()["Content-Length"])
}

func (suite *ChunkedTestSuite) TestHeaders() {
	req, err := http.NewRequest("GET", "/" + testPagesNames[0], nil)
	if err != nil {
		log.Fatal(err)
	}
	w := httptest.NewRecorder()
	suite.handler.ServeHTTP(w, req)
	suite.Equal(w.Header().Get("Content-Type"), "text/html")
	suite.Empty(w.Header()["Content-Length"])
}


func TestChunked(t *testing.T) {
	suite.Run(t, new(ChunkedTestSuite))
}

func TestStatic(t *testing.T) {
	suite.Run(t, new(StaticTestSuite))
}


func TestMain(m *testing.M) {
	flag.Parse()
	//load config from file
	log.Println("Config: " + *config_file)

	config, err := LoadConfig(*config_file)
	if err != nil {
		log.Fatal("Load config:", err)
	}

	testPagesDir = config.PageRootdir
	proxyPort  = strconv.Itoa(config.ProxyPort)
	responseForwardPort = strconv.Itoa(config.ResponseForwardPort)

	testFilesInfo, err := ioutil.ReadDir(testPagesDir)
	if err != nil {
		log.Fatal("Cannot read testfiles dir: ", err)
	}
	testPagesNames = make([]string, len(testFilesInfo))
	for i := range testFilesInfo {
		testPagesNames[i] = strings.Join([]string{testPagesDir, "/", testFilesInfo[i].Name()}, "")
	}
	log.Println("Test files: ")
	for _, n := range testPagesNames {
		log.Println(n);
	}

	proxyUrl, err = url.Parse("http://localhost:" + proxyPort)


	// //tmp random name pid file
	//	pidFile, err := ioutil.TempFile("", "nginx.pid")
	//	if err != nil {
	//		log.Fatal("Can't create temp file")
	//	}
	//	pidFileName := pidFile.Name()
	//	err = pidFile.Close()
	//	if err != nil {
	//		log.Fatal("cannot close pid.file")
	//	}

// // kill process on proxy port
//	{
//		fuserPath, err := exec.LookPath("fuser")
//		if err == nil {
//			if err := exec.Command(fuserPath, proxyPort+"/tcp").Run();
//			err == nil {
//				log.Println("Some process on proxy port. Killing...")
//				if err := exec.Command(fuserPath, "-k", proxyPort+"/tcp").Run();
//				err != nil {
//					log.Fatal("Failed to kill")
//				}
//			} else {
//				log.Println("No process on proxy port")
//			}
//		} else {
//			log.Print("cannot found fuser")
//		}
//
//	}

	log.Println("Run: nginx...")
	path, err := exec.LookPath(*nginx_exe)
	if err != nil {
		log.Fatal("nginx executable not found: ", err)
	}
	pidFileName := "/tmp/nginx.pid"
	//defer os.Remove(pidFileName)

	log.Println("Run: ", path, " -c ", *nginx_conf)
	nginxStartCmd := exec.Command(path, "-c", *nginx_conf, "-g", "pid " + pidFileName + ";")

	output, err := nginxStartCmd.StdoutPipe()
	if err != nil {
		log.Fatal("nginx run error: ", err)
	}
	erroutput, err := nginxStartCmd.StderrPipe()
	if err != nil {
		log.Fatal("nginx run error: ", err)
	}
	nginxStartCmd.Start()
	defer nginxStartCmd.Wait()
	go func() {
		io.Copy(os.Stderr, erroutput)
	}()
	go func() {
		io.Copy(os.Stdout, output)
	}()



	time.Sleep(1000 * time.Millisecond) // get nginx a second for run
	if err != nil {
		log.Fatal("nginx run error: ", err)
	}
	log.Println("Testing...")
	testRunRes := m.Run()


	log.Println("Stop: nginx...")
	nginxStopCmd := exec.Command(path, "-s", "stop", "-g", "pid " + pidFileName + ";")

	stop_output, err := nginxStopCmd.CombinedOutput()
	if err != nil {
		log.Println()
		log.Println("nginx output: ", string(stop_output))
		log.Fatal("nginx stop error: ", err)
	}
	os.Exit(testRunRes)
}
