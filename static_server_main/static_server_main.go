package main

import (
	"flag"
	"log"
	"net/http"
	"strconv"
	"bitbucket.com/go_nginx_test/static_server"
)

var config static_server.Config
var config_file = flag.String("config_file", "./config/static_server_config.json", "path to JSON config file")
//var config_file = flag.String("config_file", "/home/vladimirskipor/Development/Go/bitbucket.com/go_nginx_test/static_server_config.json", "path to JSON config file")


func main() {
	flag.Parse()
	//load config from file
	config, err := static_server.LoadConfig(*config_file)
	if err != nil {
		log.Fatal("Load config:", err)
	}



	log.Println("Config: ", config)
	serveAdress := string(config.Host + ":" + strconv.Itoa(config.Port))
	log.Println("Start static_test at ", serveAdress)

	http.HandleFunc(config.NormalPrefix,
		func(w http.ResponseWriter, r *http.Request) {
			filename := r.URL.Path[len(config.NormalPrefix):]
			static_server.ServeFile(w, r, filename)
		})

	http.HandleFunc(config.ChunkedPrefix,
		func(w http.ResponseWriter, r *http.Request) {
			filename := r.URL.Path[len(config.ChunkedPrefix):]
			static_server.ServeStatic(w, r, filename)
		})

	http.ListenAndServe(serveAdress, nil)

}
