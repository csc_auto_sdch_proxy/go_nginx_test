package static_server

import (
	"os"
//	"io"
	"log"
	"net/http"
	"encoding/json"
	"strconv"
	"io"
)

type Config struct {
	Host        string
	Port        int      `json:"port"`
	PageRootdir string
	NormalPrefix string
	ChunkedPrefix string
}



func LoadConfig(configFileName string) (*Config, error) {
	file, err := os.Open(configFileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	dec := json.NewDecoder(file)
	config := Config{}
	if err = dec.Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil
}

func ServeFile(w http.ResponseWriter, r *http.Request, filename string) {
	file, openErr := os.Open(filename)
	if openErr != nil {
		w.WriteHeader(404)
		log.Println("File open error: ", openErr)
		return
	}
	defer file.Close()

	n, copyErr := io.Copy(w, file)
	if copyErr != nil {
		w.WriteHeader(503)
		log.Println("File copy error: ", copyErr)
		return
	}
	log.Println("Sended ", n, " bytes")
//	http.ServeFile(w, r, filename)

}

func ServeStatic(w http.ResponseWriter, r *http.Request, filename string) {
	log.Println("Serve static: ", filename)
	fileInfo, err := os.Stat(filename)
	if err != nil {
		w.WriteHeader(404)
		log.Println("File stat error: ", err)
		return
	}
	w.Header().Set("Content-Length", strconv.FormatInt(fileInfo.Size(), 10))
	ServeFile(w, r, filename)
}



func StaticHandler(w http.ResponseWriter, r *http.Request) {
	ServeStatic(w, r, r.URL.Path[len("/"):])
}

func ChunkedHandler(w http.ResponseWriter, r *http.Request) {
	ServeFile(w, r, r.URL.Path[len("/"):])
}

